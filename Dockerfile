FROM alpine:latest

RUN apk --no-cache update
RUN apk --no-cache upgrade

RUN apk add --update --no-cache \
    make \
    bash \
    git \
    openssl-dev \
    vim \
    g++


# install npm
RUN apk add --update --no-cache \
    nodejs \
    npm

COPY ./package-lock.json ./
COPY ./package.json ./
COPY ./tsconfig.json ./

RUN npm install

WORKDIR /var/www/html
EXPOSE 3000

CMD ["npm", "run", "start"]


